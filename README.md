# MacGesture 2

![logo](logo.png)

Configureable global mouse gesture for Mac OS X.

# Feature

- Global mouse gesture recognition

- Filter app by their bundle name

- Configure and send shortcut by gesture

# Preview

![Preview](MacGesture.gif)

# Q&A

Feel free to open issue

# Download

Download latest zip from https://github.com/CodeFalling/MacGesture/releases

# License

Icon is design by [DanRabbit](http://www.iconarchive.com/artist/danrabbit.html) under [GNU General Public License](https://en.wikipedia.org/wiki/GNU_General_Public_License).

This project is under GNU General Public License.
